# README #

jsonFile

El presente proyecto es para el manejo de datos con base en un archivo de texto plano organizado en atributo-valor que son gestionados desde l�nea de comando. El presente proyecto ejecutable se integra de los siguientes archivos:

- [x] libs
- [x] data.cn
- [x] jsonFile.jar

# ARRANCANDO LA APLICACION #

Para iniciar la aplicaci�n ejecute la siguiente l�nea en la ventana de comandos:

> java -jar jsonFile.jar help
```
Uso: jsonFile [-opciones]

       (Ejecuta la herramienta de escritura/lectura de datos en formato json)

donde opciones incluye:
    -a, add     {'atributo':'valor'}             a�ade datos en formato json al archivo de
                                                 datos data.cn
    -l, list                                     lista los datos contenidos en el archivo de
                                                 datos data.cn
    -s, fuzzy-search  {'search':'valor'}         efectua la busqueda del valor en el archivo de
                                                 datos data.cn y despliega el registro
    -v, version                                  imprime la versi�n del producto y termina
    -?, -h, help                                 imprime este mensaje de ayuda
```
# ALMACENANDO UN DATO #

Para agregar un dato proceda con la opci�n add acompa�ada del atributo-valor que desea insertar en el archivo data.cn:

> java -jar jsonFile.jar add {"name":"Juan Akioro"}
```
jsonFile version "0.1"
(c) 2020 Todos los derechos reservados
--------------------------
Agregado Juan Akioro - OK
```
# LISTANDO LOS DATOS ALMACENADOS #

El despliegue de los datos previamente almacenados se realiza al ejecutar la siguiente l�nea en la ventana de comandos:

> java -jar jsonFile.jar list
```
jsonFile version "0.1"
(c) 2020 Todos los derechos reservados
--------------------------
[
{"name":"Ana Ruiz"}
{"name":"Juan Akioro"}
{"name":"Juan Lopez"}
{"name":"Maria Castro"}
]
```
# BUSQUEDA DE DATOS #

La b�squeda de datos mediante la aplicaci�n es mediante la siguiente l�nea:

> java -jar jsonFile.jar fuzzy-search {"search":"Juan"}
```
jsonFile version "0.1"
(c) 2020 Todos los derechos reservados
--------------------------
Buscando valor: Juan
{"name":"Juan Lopez"}
{"name":"Juan Akioro"}
```
El algoritmo de b�squeda es una aproximaci�n basada en el algoritmo de ordenamiento QuickSort permitiendo encontrar los registros que coincidan parcial o totalmente independiente de mayusculas o minusculas, desplegando todos los registros donde se encontr� coincidencia.
