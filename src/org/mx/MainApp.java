package org.mx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Vector;

import org.json.JSONObject;
import org.mx.complements.ConsoleColor;
import org.mx.dataStore.jsonFileRow;

public class MainApp {
	//Variables generales
	private static String version = "0.1";
	private static String anio = "2020";
	private static String dataFileName = "data.cn";
	private static Vector<String> founded = new Vector<>();
	
	public static void main(String[] args) {
		if (args.length > 0){
			if (args[0].equals("-v") || args[0].equals("version")){
				System.out.println("jsonFile version \""+version+"\"");
				System.out.println("(c) "+anio+" Todos los derechos reservados");
				System.out.println(" ");
				System.out.println("v0.1");
				System.out.println("    - versi�n inicial que contempla escritura/lectura de datos");
			}else if (args[0].equals("-h") || args[0].equals("-?") || args[0].equals("help")){
				System.out.println("Uso: jsonFile [-opciones]");
				System.out.println(" ");
				System.out.println("       (Ejecuta la herramienta de escritura/lectura de datos en formato json)");
				System.out.println(" ");
				System.out.println("donde opciones incluye:");
				System.out.println("    -a, add     {'atributo':'valor'[,'atributo':'valor']}   a�ade datos en formato json al archivo de");
				System.out.println("                                                            datos data.cn");
				System.out.println("    -l, list                                                lista los datos contenidos en el archivo de");
				System.out.println("                                                            datos data.cn");
				System.out.println("    -s, fuzzy-search  {'search':'valor'}                    efectua la busqueda del valor en el archivo de");
				System.out.println("                                                            datos data.cn y despliega el registro");
				System.out.println("    -v, version                                             imprime la versi�n del producto y termina");
				System.out.println("    -?, -h, help                                            imprime este mensaje de ayuda");
			}else if (args[0].equals("-a") || args[0].equals("add")){
				String value = "";
				//Fusionando los valores por si vienen con algun espacio
				for (int c=1; c < args.length; c++) {
					value += args[c];
				}
				MainApp mainApp = new MainApp();
				mainApp.add(value);
			}else if (args[0].equals("-l") || args[0].equals("list")){
				MainApp mainApp = new MainApp();
				mainApp.list();
			}else if (args[0].equals("-s") || args[0].equals("fuzzy-search")){
				String value = "";
				//Fusionando los valores por si vienen con algun espacio
				for (int c=1; c < args.length; c++) {
					value += args[c];
				}
				MainApp mainApp = new MainApp();
				mainApp.search(value);
			} else {
				System.out.println("jsonFile version \""+version+"\"");
				System.out.println("(c) "+anio+" Todos los derechos reservados");
				System.out.println(" ");
				System.out.println("�Par�metro desconocido! Ejecute \"jsonFile help\" para obtener ayuda");
			}
		}else {
			System.out.println("jsonFile version \""+version+"\"");
			System.out.println("(c) "+anio+" Todos los derechos reservados");
			System.out.println(" ");
			System.out.println("�Sin actividad a procesar!");
		}
	}
	
	/**
     * Proceso de agregado de elementos al archivo data
     * 
     * @param value es la cadena json a procesar
     * @return boolean indicando si fue exitoso o no el proceso
     */
	public boolean add(String value){
		boolean result = false;
		boolean isLinux = false;
		//Detectando la ubicaci�n del archivo
		String currentDirectory = Paths.get(".").toAbsolutePath().normalize().toString();
		String pathSeparator = "\\"; //Windows path
		if (currentDirectory.indexOf("/") >= 0){
			//Linux path
			pathSeparator = "/";
			isLinux = true;
		}
		System.out.println("jsonFile version \""+version+"\"");
		System.out.println("(c) "+anio+" Todos los derechos reservados");
		System.out.println("--------------------------");
		//No indico datos no se hace nada
		if (value == "") {
			if (isLinux){
				System.out.println(ConsoleColor.RED+"ADVERTENCIA"+ConsoleColor.RESET+" - No indic� informaci�n a agregar");
			}else{
				System.out.println("ADVERTENCIA - No indic� informaci�n a agregar");
			}
		//Hay datos se procede a insertarlo en el archivo
		}else{
			try {
				//Abriendo el archivo de datos
				String destino = currentDirectory+pathSeparator+dataFileName;
				File file = new File (destino);
				if (!file.exists()) {
					System.out.println("Creando archivo "+dataFileName);
				}
				FileWriter fwMain = new FileWriter(destino, true);
				//Convirtiendo a objeto json la cadena de entrada json
				JSONObject jsonObj = new JSONObject(value);
				jsonObj.keySet().forEach(keyStr ->
			    {
			        Object keyvalue = jsonObj.get(keyStr);
			        System.out.println("Agregado " + keyvalue + " - OK");
			        //Escribiendo en el archivo de datos
			        String rowData = keyStr+"="+keyvalue+"\r\n";
			        try {
						fwMain.write(rowData,0,rowData.length());
					} catch (IOException err) {
						System.out.println("ERROR I/O...");
						System.out.println("   Fallo "+err);
					}
			    });
				fwMain.close();
				result = true;
			} catch (IOException err) {
				System.out.println("ERROR I/O...");
				System.out.println("   Fallo "+err);
				return false;
			} catch (Exception err) {
				System.out.println("ERROR...");
				System.out.println("   Fallo inesperado "+err);
				return false;
			} finally {
				
			}
		}
		return result;
	}
	
	/**
     * Proceso de listado de elementos del archivo data
     * 
     * @param ninguno
     * @return boolean indicando si fue exitoso o no el proceso
     */
	public boolean list(){
		boolean result = false;
		boolean isLinux = false;
		//Detectando la ubicaci�n del archivo
		String currentDirectory = Paths.get(".").toAbsolutePath().normalize().toString();
		String pathSeparator = "\\"; //Windows path
		if (currentDirectory.indexOf("/") >= 0){
			//Linux path
			pathSeparator = "/";
			isLinux = true;
		}
		System.out.println("jsonFile version \""+version+"\"");
		System.out.println("(c) "+anio+" Todos los derechos reservados");
		System.out.println("--------------------------");
		try {
			//Abriendo el archivo de datos
			String destino = currentDirectory+pathSeparator+dataFileName;
			File file = new File (destino);
			if (!file.exists()) {
				if (isLinux){
					System.out.println(ConsoleColor.RED+"ADVERTENCIA"+ConsoleColor.RESET+" - No existe el archivo de datos "+dataFileName);
				}else{
					System.out.println("ADVERTENCIA - No existe el archivo de datos "+dataFileName);
				}
			}else{
				InputStreamReader data = new InputStreamReader(new FileInputStream(file.getName()), "UTF-8");
				BufferedReader rowData = new BufferedReader(data);
				String line = "";
				//Recuperando la informaci�n del archivo de datos y pasandola a un contenedor
				Vector<jsonFileRow> contenedor = new Vector<>();
				while ((line = rowData.readLine()) != null){
					String[] items = line.split("=");
					if (items.length == 2) {
						contenedor.addElement(new jsonFileRow(items[0],items[1]));
					}
				}
				//Ordenando la informaci�n
				quickSort(contenedor, 0, contenedor.size() - 1);
				//Desplegando la informaci�n
				System.out.print("[");
				contenedor.forEach((n) -> {
					boolean firstRow = false;
					if (!firstRow) {
						System.out.print("\r\n{\"" + n.getAtributo() + "\":\"" + n.getValor() + "\"}");
						firstRow = true;
					}else{
						System.out.print(",\r\n{\"" + n.getAtributo() + "\":\"" + n.getValor() + "\"}");
					}
				});
				if (contenedor.size() > 0) {
					System.out.print("\r\n");
				}
				System.out.print("]");
				data.close();
				result = true;
			}	
		} catch (IOException err) {
			System.out.println("ERROR I/O...");
			System.out.println("   Fallo "+err);
			return false;
		} catch (Exception err) {
			System.out.println("ERROR...");
			System.out.println("   Fallo inesperado "+err);
			return false;
		} finally {
			
		}
		return result;
	}
	
	/**
     * Proceso de busqueda de elemento en el archivo data
     * 
     * @param ninguno
     * @return boolean indicando si fue exitoso o no el proceso
     */
	public boolean search(String value){
		boolean result = false;
		boolean isLinux = false;
		//Detectando la ubicaci�n del archivo
		String currentDirectory = Paths.get(".").toAbsolutePath().normalize().toString();
		String pathSeparator = "\\"; //Windows path
		if (currentDirectory.indexOf("/") >= 0){
			//Linux path
			pathSeparator = "/";
			isLinux = true;
		}
		System.out.println("jsonFile version \""+version+"\"");
		System.out.println("(c) "+anio+" Todos los derechos reservados");
		System.out.println("--------------------------");
		//No indico datos no se hace nada
		if (value == "") {
			if (isLinux){
				System.out.println(ConsoleColor.RED+"ADVERTENCIA"+ConsoleColor.RESET+" - No indic� informaci�n a buscar");
			}else{
				System.out.println("ADVERTENCIA - No indic� informaci�n a buscar");
			}
		//Hay datos se procede a buscarlo en el archivo
		}else{
			try {
				//Abriendo el archivo de datos
				String destino = currentDirectory+pathSeparator+dataFileName;
				File file = new File (destino);
				if (!file.exists()) {
					if (isLinux){
						System.out.println(ConsoleColor.RED+"ADVERTENCIA"+ConsoleColor.RESET+" - No existe el archivo de datos "+dataFileName);
					}else{
						System.out.println("ADVERTENCIA - No existe el archivo de datos "+dataFileName);
					}
				}else{
					//Recuperando la informaci�n
					InputStreamReader data = new InputStreamReader(new FileInputStream(file.getName()), "UTF-8");
					BufferedReader rowData = new BufferedReader(data);
					String line = "";
					//Recuperando la informaci�n del archivo de datos y pasandola a un contenedor
					Vector<jsonFileRow> contenedor = new Vector<>();
					while ((line = rowData.readLine()) != null){
						String[] items = line.split("=");
						if (items.length == 2) {
							contenedor.addElement(new jsonFileRow(items[0],items[1]));
						}
					}
					//Convirtiendo a objeto json la cadena de entrada json
					JSONObject jsonObj = new JSONObject(value);
					jsonObj.keySet().forEach(keyStr ->
				    {
				        Object keyvalue = jsonObj.get(keyStr);
				        System.out.println("Buscando valor: " + keyvalue);
				        if (keyStr.equals("search")) {
				        	founded = new Vector<>();
				        	//Buscando el keyvalue!!
				        	quickSearch(contenedor, keyvalue.toString(), 0, contenedor.size() - 1);
				        	if (founded.size() == 0) {
				        		System.out.println("Sin coincidencias");
				        	}
				        }
				    });
				}
			} catch (IOException err) {
				System.out.println("ERROR I/O...");
				System.out.println("   Fallo "+err);
				return false;
			} catch (Exception err) {
				System.out.println("ERROR...");
				System.out.println("   Fallo inesperado "+err);
				return false;
			} finally {
				
			}
		}
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                                                  FUNCIONES PRIVADAS
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
     * Comparador de cadenas
     * 
     * @param String str1, String str2 que son las cadenas a comparar
     * @return int que indica si el String str1 es menor (negativo) o mayor (positivo)
     */
	private static int stringCompare(String str1, String str2) { 
        int l1 = str1.length(); 
        int l2 = str2.length(); 
        int lmin = Math.min(l1, l2); 
        for (int i = 0; i < lmin; i++) { 
            int str1_ch = str1.charAt(i); 
            int str2_ch = str2.charAt(i); 
            if (str1_ch != str2_ch) { 
                return str1_ch - str2_ch; 
            } 
        } 
        if (l1 != l2) { 
            return l1 - l2; 
        } else { 
	        return 0;
	    } 
	}
	private static int partition(Vector<jsonFileRow> entrada, int begin, int end) {
	    int pivot = end;
	    int counter = begin;
	    for (int i = begin; i < end; i++) {
	        if (stringCompare(entrada.elementAt(i).getValor(),entrada.elementAt(pivot).getValor()) < 0) {
	        	jsonFileRow temp = entrada.elementAt(counter);
	            entrada.setElementAt(entrada.elementAt(i),counter);
	            entrada.setElementAt(temp, i);
	            counter++;
	        }
	    }
	    jsonFileRow temp = entrada.elementAt(pivot);
	    entrada.setElementAt(entrada.elementAt(counter),pivot);
        entrada.setElementAt(temp, counter);
	    return counter;
	}
	/**
     * M�todo de ordenamiento QuickSort
     * 
     * @param Vector de datos a ordenar, int valor de indice inicial, int valor de indice final
     * @return ninguno
     */
	private static void quickSort(Vector<jsonFileRow> entrada, int begin, int end) {
	    if (end <= begin) return;
	    int pivot = partition(entrada, begin, end);
	    quickSort(entrada, begin, pivot-1);
	    quickSort(entrada, pivot+1, end);
	}
	//--------------
	private static int partitionSearch(Vector<jsonFileRow> entrada, String key, int begin, int end) {
	    int pivot = end;
	    int counter = begin;
	    for (int i = begin; i < end; i++) {
	    	//Buscando el key en el item din�mico
	    	if (entrada.elementAt(i).getValor().toLowerCase().indexOf(key.toLowerCase()) >= 0) {
	    		if (!founded.contains(entrada.elementAt(i).getValor())) {
		    		founded.addElement(entrada.elementAt(i).getValor());
		    		System.out.println("{\""+entrada.elementAt(i).getAtributo()+"\":\""+entrada.elementAt(i).getValor()+"\"}");
	    		}
	    	}
	    	//Acomodando
	        if (stringCompare(entrada.elementAt(i).getValor(),entrada.elementAt(pivot).getValor()) < 0) {
	        	jsonFileRow temp = entrada.elementAt(counter);
	            entrada.setElementAt(entrada.elementAt(i),counter);
	            entrada.setElementAt(temp, i);
	            counter++;
	        }
	    }
	    //Buscando el key en el item pivote
	    if (entrada.elementAt(pivot).getValor().toLowerCase().indexOf(key.toLowerCase()) >= 0) {
    		if (!founded.contains(entrada.elementAt(pivot).getValor())) {
	    		founded.addElement(entrada.elementAt(pivot).getValor());
	    		System.out.println("{\""+entrada.elementAt(pivot).getAtributo()+"\":\""+entrada.elementAt(pivot).getValor()+"\"}");
    		}
    	}
	    jsonFileRow temp = entrada.elementAt(pivot);
	    entrada.setElementAt(entrada.elementAt(counter),pivot);
        entrada.setElementAt(temp, counter);
	    return counter;
	}
	/**
     * M�todo de busqueda derivado de QuickSort
     * 
     * @param Vector de datos a ordenar, int valor de indice inicial, int valor de indice final
     * @return ninguno
     */
	private static void quickSearch(Vector<jsonFileRow> entrada, String key, int begin, int end) {
	    if (end <= begin) return;
	    int pivot = partitionSearch(entrada, key, begin, end);
	    quickSearch(entrada, key, begin, pivot-1);
	    quickSearch(entrada, key, pivot+1, end);
	}
}