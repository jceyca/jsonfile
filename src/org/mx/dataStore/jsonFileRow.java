package org.mx.dataStore;

public class jsonFileRow {
	//Atributos
	private String atributo;
	private String valor;
	
	//Constructores
	public jsonFileRow() {
	}
	public jsonFileRow(String atributo, String valor) {
		this.atributo = atributo;
		this.valor = valor;
	}

	public String getAtributo() {
		return atributo;
	}
	public void setAtributo(String atributo) {
		this.atributo = atributo;
	}
	
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}
